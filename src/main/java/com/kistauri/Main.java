package com.kistauri;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.*;
import com.mongodb.client.model.Filters;
import org.bson.Document;

public class Main {

    public static void main(String[] args){
        String student_query = "";
        System.out.println(getStudentsList(student_query));
    }

    private static String getUri(){
        String uri = "mongodb://";
        uri += System.getenv("MONGO_USERNAME");
        uri += ":";
        uri += System.getenv("MONGO_PASSWORD");
        uri += "@";
        uri += System.getenv("MONGO_HOST");
        return uri;
    }

    private static String getStudentsList(String student_query){
        try (MongoClient mongoClient = new MongoClient(new MongoClientURI(getUri()))) {
            MongoDatabase database = mongoClient.getDatabase(System.getenv("MONGO_DB_NAME"));
            MongoCollection<Document> collection = database.getCollection(System.getenv("MONGO_COLLECTION_NAME"));

            Document doc = collection.find(Filters.eq("Mark", 2)).first();

            student_query = doc.toString();
        }
        catch (Exception e){
            System.err.println(e.getMessage());
        }
        return student_query;
    }
}
